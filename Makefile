#
# makefile for sccs2rcs
#
VERS=$(shell sed <sccs2rcs -n -e '/version=\(.*\)/s//\1/p')

SOURCES = README COPYING NEWS sccs2rcs sccs2rcs.xml Makefile control

all: sccs2rcs.1

sccs2rcs.1: sccs2rcs.xml
	xmlto man sccs2rcs.xml

sccs2rcs.html: sccs2rcs.xml
	xmlto html-nochunks sccs2rcs.xml

clean:
	rm -f  *~ *.1 *.html *.tar.gz MANIFEST
	rm -fr .rs* typescript test/typescript

sccs2rcs-$(VERS).tar.gz: $(SOURCES) sccs2rcs.1 
	@ls $(SOURCES) sccs2rcs.1 | sed s:^:sccs2rcs-$(VERS)/: >MANIFEST
	@(cd ..; ln -s sccs2rcs sccs2rcs-$(VERS))
	(cd ..; tar -czvf sccs2rcs/sccs2rcs-$(VERS).tar.gz `cat sccs2rcs/MANIFEST`)
	@(cd ..; rm sccs2rcs-$(VERS))

COMMON_PYLINT = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
PYLINTOPTS = "C0103,C0301,C0326,C0111,W0110,W0141,W0311,W0312,W0603"
pylint:
	@pylint $(COMMON_PYLINT) --disable=$(PYLINTOPTS) sccs2rcs

version:
	@echo $(VERS)

dist: sccs2rcs-$(VERS).tar.gz

release: sccs2rcs-$(VERS).tar.gz sccs2rcs.html
	shipper version=$(VERS) | sh -e -x

refresh: sccs2rcs.html
	shipper -N -w version=$(VERS) | sh -e -x
